The present python script "parse_logs.py" allows one to parse Details! data and saves the result into .csv file which can be read by any excel-type program.

To do the thing, one can go by the following protocol:
1. In game, choose the desired metrics (Damage done or Healing)
2. Select the desired fight (specific bosses) or the total data
3. Export the data with the option "copy", and paste the details! data into a text file
4. If you want to compare the metrics based on a several fights, paste them one by one into a single text file
5. Run the python script
6. The csv file should be created and ready for the analysis

<Storm> Veetilk - Flamelash EU
