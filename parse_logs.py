# IMPORT
import datetime
import time
import csv
# Library for dialogue windows construction
import tkinter as tk
from tkinter import filedialog # Allows to choose files via standart windows dialogue
#
def readFile(fileName) :
    '''
    Read ASCII file line by line
    With '\t' deliminers
    Empty lines and '#' commented lines are skipped
    '''
    inputFile = open(fileName, errors = 'ignore')
    print("Reading the file:\t"+fileName)
    # read file line by line
    data = []
    buff = inputFile.readline()
    while (buff != '') :
        # if the line is not commented and not empty
        if (buff[0] != '#') and (buff[0] != '\n'):
            buff = buff.split('\t')
            data += buff
        # read new line
        buff = inputFile.readline()
    # Finish reading
    inputFile.close()
    # Return
    return data
#
def get_offset(log) :
    '''
    Find an offset (there is in case of getting the data from someone)
    First line in the log must be the info line from details
    Using "Details!:" as an ancor
    '''
    offset = log[0].split() # Get the first line
    offset = offset.index("Details!:") # Get the index offset
    return offset
#
def read_number(string) :
    """
    """
    #
    if (string[-1] == 'K') :
        return float(string[:-1])*1e3
    elif (string[-1] == 'M') :
        return float(string[:-1])*1e6
    else :
        return float(string)
#
def parse_log(log) :
    '''
    '''
    # Determine the offset
    offset = get_offset(log)
    # Column positions
    i_type = 1 + offset
    i_name = 1 + offset
    f_boss = lambda line : ((line.split("for"))[1]).split()[0]
    i_duration = -2
    f_value = ({
            "Damage" : lambda line, duration : read_number(line[-3])/duration,
            "Healing" : lambda line, duration : read_number(line[-3])/duration,
            "DPS" : lambda line, duration : float(line[-2])
            })
    # Type of metrics
    metrics = (log[0].split())[i_type]
    # Collect the total list of players
    players_list = ["boss"]
    for index in range(len(log)) :
        line = log[index]
        line = line.split()
        if (line[offset] != "Details!:") :
            name = line[i_name]
            if name not in players_list :
                players_list += [name]
            #
        #
    #
    # Read and parse the data
    data = {}
    for index in range(len(log)) :
        # Read the next line
        line = log[index]
        line = line.split()
        # Parse it
        if (line[offset] == "Details!:") :
            # boss name
            boss = f_boss(log[index])
            # time
            duration = line[i_duration][1:]
            duration = time.strptime(duration.split(',')[0],'%M:%S') # transform time into seconds
            duration = datetime.timedelta(minutes=duration.tm_min,seconds=duration.tm_sec).total_seconds()
            #
            data[boss] = {}
        else :
            name = line[i_name] # player name
            value = f_value[metrics](line, duration)
            #
            data[boss][name] = value # DPS or HPS
        #
    #
    return (metrics, players_list, data)
#
def parse_combined_log(log) :
    '''
    Split a combined log with different types (damage, heal etc) into several
    Parse each single-type log separately
    '''
    # Determine the offset
    offset = get_offset(log)
    # Column positions
    i_type = 1 + offset
    # Determine types of metrics: damage or healing or ...
    metrics_list = []
    for index in range(len(log)) :
        line = log[index]
        line = line.split()
        if (line[offset] == "Details!:") :
            metrics = line[i_type]
            if metrics not in metrics_list :
                metrics_list += [metrics]
            #
        #
    #
    # Split the log into several parts for different metrics
    log_list = dict.fromkeys(metrics_list)
    for metrics in metrics_list :
        log_list[metrics] = []
        #
    for index in range(len(log)) :
        line = log[index]
        line = line.split()
        if (line[offset] == "Details!:") :
            metrics = line[i_type]
        #
        log_list[metrics] += [log[index]]
    #
    # Read logs one by one for different metrics
    parsed_logs = []
    for metrics in metrics_list :
        parsed_logs += [parse_log(log_list[metrics])]
    #
    return parsed_logs
#
def dict2csv (csv_fileName, csv_columns, dict_data) :
    '''
    Make save dictionary as csv file
    Taken from the depth of stackOverflow
    '''
    try:
        with open(csv_fileName, 'w', newline = '') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames = csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
            #
        #
    except IOError:
        print("I/O error")
    #
    return True
#
'''
MAIN FUNCTION
'''
# Create the tk window for getting the file name(s)
root = tk.Tk()
root.withdraw() # Hide the master window
# Open the file dialog to choose the files
fileName_list = filedialog.askopenfilenames()
# Destroy the tk window
root.destroy()
#
# Load files one by one
parsed_logs_combined = {}
names_combined = {}
for fileName in fileName_list :
    # Read the log file
    log = readFile(fileName)
    # Parse the log
    parsed_logs = parse_combined_log(log)
    # Compare the parsed log to the already accumulated data
    for log in parsed_logs :
        # Local parameters of each newly parsed log
        (metrics, names, data) = log
        # Check if a certain type of metrics is already in the accumulated data
        # If yes, make a comparison
        # If no, just write it to the data
        if metrics in names_combined.keys() :
            # First check the names
            for player in names :
                if (player not in names_combined[metrics]) :
                    # Add the player name to the combined list
                    names_combined[metrics] += [player]
                #
            #
            # Go through the data boss by boss
            for boss in data.keys() :
                # Check if the data on a particular boss have been recorded
                if (boss in parsed_logs_combined[metrics].keys()):
                    for player in data[boss].keys() :
                        if (player in parsed_logs_combined[metrics][boss].keys()) :
                            parsed_logs_combined[metrics][boss][player] = max(parsed_logs_combined[metrics][boss][player], data[boss][player])
                        else :
                            parsed_logs_combined[metrics][boss][player] = data[boss][player]
                        #
                    #
                else :
                    parsed_logs_combined[metrics][boss] = data[boss]
                #
            #
        else :
            names_combined[metrics] = names
            parsed_logs_combined[metrics] = data
        #
    #
#
# Save the finalized dictionary
for metrics in parsed_logs_combined.keys() :
    # Each log has to be transformed from nested dictionary into a list of dictionaries
    # Then it can be saved into a csv file using "standart" function
    parsed_logs = parsed_logs_combined[metrics]
    columns = names_combined[metrics]
    #
    data = []
    for boss in list(parsed_logs.keys()) :
        data += [{"boss" : boss}]
        data[-1].update(parsed_logs[boss])
    #
    dict2csv (metrics+".csv", columns, data)
#
